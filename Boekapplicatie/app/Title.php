<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    protected $fillable = [
        'book_name', 'content', 'comment', 'uitgeleend'
    ];
    public function authors()
    {
        return $this->hasMany('App\Author', 'id');
    }

    public function publishers()
    {
        return $this->hasMany('App\Publisher', 'id');
    }
}
