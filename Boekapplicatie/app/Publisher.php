<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $fillable = ['title_id', 'name'];

    public function title()
    {
        return $this->belongsTo('App\Title', 'id', 'title_id');
    }
}
